import React, {useEffect, useRef} from 'react';
import {fetchEstablishmentRequest} from "../../store/actions/establishmentsActions";
import {useDispatch, useSelector} from "react-redux";
import {CircularProgress, Grid} from "@material-ui/core";
import EstablishmentItem from "../../components/EstablishmentItem/EstablishmentItem";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {addEstablishment} from "../../paths";
import {Link} from "react-router-dom";

const CafeCritics = () => {

    const dispatch = useDispatch();

    const messagesEndRef = useRef(null);

    const establishments = useSelector(state => state.establishments.establishment);
    const fetchLoading = useSelector(state => state.establishments.fetchLoading);

    const user = useSelector(state => state.users.user);

    useEffect(() => {
        window.scrollTo(0, 0);
        dispatch(fetchEstablishmentRequest());
    }, [dispatch, messagesEndRef]);

    const addLoading = useSelector(state => state.establishments.addLoading);

    return (
        <div>
            <p>All places</p>

            <Grid item container justifyContent="space-between" alignItems="center">

                {user ?
                    <ButtonWithProgress
                        component={Link}
                        to={addEstablishment}
                        type="submit"
                        variant="contained"
                        color="secondary"
                        loading={addLoading}
                        disabled={addLoading}
                    >
                        Add new place
                    </ButtonWithProgress> :
                    ''}

            </Grid>

            <Grid item>
                <Grid item container justifyContent="center" direction="row" spacing={1}>
                    {fetchLoading ? (
                        <Grid container justifyContent="center" alignItems="center">
                            <Grid item>
                                <CircularProgress/>
                            </Grid>
                        </Grid>
                    ) : establishments.map(establishment => (
                        <EstablishmentItem
                            key={establishment._id}
                            id={establishment._id}
                            title={establishment.title}
                            image={establishment.image}
                            user={establishment.user}
                            overallReviews={establishment.overallReviews}
                        />
                    ))}
                </Grid>
            </Grid>
        </div>
    );
};

export default CafeCritics;