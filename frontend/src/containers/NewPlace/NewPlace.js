import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Typography} from "@material-ui/core";
import PlaceForm from "../../components/PlaceForm/PlaceForm";
import {addEstablishmentRequest} from "../../store/actions/establishmentsActions";

const NewPlace = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.establishments.addError);
    const loading = useSelector(state => state.establishments.addLoading);

    const onSubmit = placeData => {
        console.log(placeData)
        dispatch(addEstablishmentRequest(placeData));
    };

    return (
        <>
            <Typography variant="h4">Add new place</Typography>
            <PlaceForm
                onSubmit={onSubmit}
                error={error}
                loading={loading}
            />
        </>
    );
};

export default NewPlace;