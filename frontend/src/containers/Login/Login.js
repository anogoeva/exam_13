import React, {useEffect, useRef, useState} from 'react';
import {Link as RouterLink, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";

import LockOpenOutlinedIcon from '@material-ui/icons/LockOpenOutlined';
import {Avatar, Container, Grid, Link, makeStyles, Typography} from "@material-ui/core";
import {clearError, loginUser} from "../../store/actions/usersActions";
import {Alert} from "@material-ui/lab";
import ButtonWithProgress from "../../components/UI/ButtonWithProgress/ButtonWithProgress";
import {InputAdornment} from "@mui/material";
import VisibilityIcon from "@mui/icons-material/Visibility";
import VisibilityOffIcon from "@mui/icons-material/VisibilityOff";

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  alert: {
    marginTop: theme.spacing(3),
    width: "100%",
  },
}));

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const error = useSelector(state => state.users.loginError);
  const loading = useSelector(state => state.users.loginLoading);
  const navigate = useNavigate();

  const [user, setUser] = useState({
    username: '',
    password: ''
  });

  const [isPasswordVisible, setIsPasswordVisible] = useState(false);

  const messagesEndRef = useRef(null);

  useEffect(() => {
    if (!!messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView({
        behavior: 'smooth'
      }, 200);
    }
    return () => {
      dispatch(clearError());
    };
  }, [dispatch, messagesEndRef]);

  const inputChangeHandler = e => {
    const {name, value} = e.target;

    setUser(prevState => ({...prevState, [name]: value}));
  };

  const submitFormHandler = e => {
    e.preventDefault();
    dispatch(loginUser({...user, navigate}));
  };

  const getFieldError = fieldName => {
    try {
      return error.errors[fieldName].message;
    } catch (e) {
      return undefined;
    }
  };

  return (
    <Container component="section" maxWidth="xs">
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOpenOutlinedIcon/>
        </Avatar>
        <Typography component="h1" variant="h6">
          Log in
        </Typography>
        {
          error &&
          <Alert severity="error" className={classes.alert}>
              {error.message || error.global}
          </Alert>
        }
        <Grid
          component="form"
          container
          className={classes.form}
          onSubmit={submitFormHandler}
          spacing={2}
        >
          <FormElement
            type="text"
            autoComplete="new-username"
            label="Username"
            name="username"
            value={user.username}
            onChange={inputChangeHandler}
            error={getFieldError('username')}
          />

          <FormElement
              type={isPasswordVisible ? "text" : "password"}
              InputProps={{
                endAdornment: (
                    <InputAdornment position="end">
                      {isPasswordVisible ? (
                          <VisibilityIcon
                              onClick={() => setIsPasswordVisible(!isPasswordVisible)}/>
                      ) : (
                          <VisibilityOffIcon onClick={() => setIsPasswordVisible(!isPasswordVisible)}/>
                      )}
                    </InputAdornment>
                )
              }}
              autoComplete="current-password"
              label="Пароль"
              name="password"
              value={user.password}
              required={true}
              onChange={inputChangeHandler}
          />

          <Grid item xs={12}>
            <ButtonWithProgress
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              loading={loading}
              disabled={loading}
            >
              Log in
            </ButtonWithProgress>
          </Grid>

          <Grid item container justifyContent="flex-end">
            <Link component={RouterLink} variant="body2" to="/register">
              Or sign up
            </Link>
          </Grid>
        </Grid>
      </div>
    </Container>
  );
};

export default Login;