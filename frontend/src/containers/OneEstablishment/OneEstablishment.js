import React, {useEffect, useRef} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchOneEstablishmentRequest} from "../../store/actions/establishmentsActions";
import {useParams} from "react-router-dom";
import {apiURL} from "../../config";
import Container from "@mui/material/Container";
import {makeStyles} from "@mui/styles";
import {createTheme} from "@mui/material/styles";
import {fetchImageCafeRequest} from "../../store/actions/imagesCafeActions";
import {fetchReviewRequest} from "../../store/actions/reviewsActions";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import {Divider, Rating} from "@mui/material";
import UploadPhoto from "../../components/UploadPhoto/UploadPhoto";
import CommentForm from "../../components/CommentForm/CommentForm";

const theme = createTheme({
    breakpoints: {
        values: {
            sm: 767,
        },
    },
});

const useStyles = makeStyles(() => ({
    container: {
        paddingTop: '170px',
        [theme.breakpoints.down('sm')]: {
            paddingTop: '100px',
        },
    },

    establishmentTitle: {
        textAlign: 'center',
        padding: "0 30px",
        fontSize: '22px',
        fontWeight: '500',
        lineHeight: '30px',
        wordWrap: 'break-word',
    },

    establishmentText: {
        padding: '0 20px',
    },

    establishmentBlock: {
        maxWidth: '80%',
        textAlign: 'center',
        padding: '20px 10px',
        margin: "15px auto",
        boxShadow: 'rgba(60, 64, 67, 0.3) 0px 1px 2px 0px, rgba(60, 64, 67, 0.15) 0px 2px 6px 2px',
        borderRadius: '10px',
        [theme.breakpoints.down('sm')]: {
            margin: '10px auto',
            padding: '10px',
            maxWidth: '95%',
        },
    },

    establishmentImageBlock: {
        display: "flex",
        justifyContent: "center",
        maxWidth: '90%'
    },

    establishmentImage: {
        margin: '20px 0',
        maxWidth: '100%',
        height: 'auto',
    },
    root: {
        minWidth: "100%",
        minHeight: "100vh",
        display: "flex",
        flexDirection: "column",
        justifyContent: "center"
    },
}));


const OneEstablishment = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const establishment = useSelector(state => state.establishments.oneEstablishment);
    const {id} = useParams();
    const messagesEndRef = useRef(null);
    const imagesCafe = useSelector(state => state.imageCafes.imageCafe);
    const reviews = useSelector(state => state.reviews.review);
    const user = useSelector(state => state.users.user);

    useEffect(() => {
        if (!!messagesEndRef.current) {
            messagesEndRef.current.scrollIntoView({
                behavior: 'smooth'
            }, 200);
        }
        dispatch(fetchOneEstablishmentRequest(id));
        dispatch(fetchImageCafeRequest('?establishment=' + id));
        dispatch(fetchReviewRequest('?establishment=' + id));
    }, [dispatch, messagesEndRef, id]);

    return (
        <Container maxWidth="lg" ref={messagesEndRef} className={classes.container}>
            {establishment && (
                <div className={classes.establishmentBlock}>
                    <div>
                        <h4 className={classes.establishmentTitle}>{establishment.title}</h4>
                        <div id='description' className={establishment.description}
                             dangerouslySetInnerHTML={{__html: establishment.description}}/>
                    </div>
                    <div className={classes.establishmentImageBlock}>
                        {establishment?.image ?
                            <img className={classes.establishmentImage} src={apiURL + '/' + establishment.image}
                                 alt={'establishment'}/> : null}
                    </div>

                    {
                        imagesCafe.map(image => (
                            <img key={image._id} src={apiURL + '/' + image.image} width="100px" height="70px"
                                 alt="галерея"/>
                        ))
                    }

                    <p><b>{'Overall: ' + establishment.overallReviews}</b> <Rating name="foodReviews" value={establishment.overallReviews || null} readOnly /></p>
                    <p>{'Quality of food: ' + establishment.foodReviews} <Rating name="foodReviews" value={establishment.foodReviews || null} readOnly /></p>
                    <p>{'Service quality: ' + establishment.serviceReviews} <Rating name="foodReviews" value={establishment.serviceReviews || null} readOnly /></p>
                    <p>{'Interior quality: ' + establishment.interiorReviews} <Rating name="foodReviews" value={establishment.interiorReviews || null} readOnly /></p>

                    {/*_id(pin):"623f106b90c553a9d5b4ab3e"*/}
                    {/*comment(pin):"fdsafdsafdsa fdsafdsafdsa"*/}
                    {/*establishment(pin):"623f0a92183c967ec491dbe9"*/}
                    {/*user(pin):"623f09891421683a1c30f5a1"*/}
                    {/*foodQuality(pin):5*/}
                    {/*serviceQuality(pin):5*/}
                    {/*interiorQuality(pin):4*/}
                    {/*dateTime(pin):"2022-03-26T13:08:47.548Z"*/}
                    {/*__v(pin):0*/}

                    <Grid container>
                        <Typography inline="true" variant="body2" align="left" fontWeight="bold">Reviews: </Typography>
                        <hr/>
                        {
                            reviews.map(review => (
                                <Grid item xs={12} key={review._id}>
                                    <Typography inline="true" variant="body2"
                                                align="left">Date: {review.dateTime}</Typography>
                                    <Typography inline="true" variant="body2"
                                                align="left">User: {review.user.username}</Typography>
                                    <Typography inline="true" variant="body2"
                                                align="left">Said: {review.comment}</Typography>
                                    <Typography inline="true" variant="body2" align="left">Quality of
                                        food: {review.foodQuality}</Typography>
                                    <Typography inline="true" variant="body2" align="left">Service
                                        quality: {review.serviceQuality}</Typography>
                                    <Typography inline="true" variant="body2" align="left">Interior
                                        quality: {review.interiorQuality}</Typography>
                                    <Divider/>
                                    <Divider/>
                                </Grid>
                            ))
                        }
                    </Grid>

                    {user ? <CommentForm/> : ''}

                    {user ? <UploadPhoto/> : ''}

                </div>
            )}
        </Container>
    );
};

export default OneEstablishment;