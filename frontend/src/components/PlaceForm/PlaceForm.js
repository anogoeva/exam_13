import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import FormElement from "../UI/Form/FormElement";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

const PlaceForm = ({onSubmit, error, loading}) => {
    const classes = useStyles();

    const [state, setState] = useState({
        title: "",
        description: "",
        image: null,
    });

    // const [checkbox, setCheckbox] = useState();

    // useEffect(() => {
    //     return () => {
    //         dispatch(clearErrorPlace());
    //     };
    // }, [dispatch]);

    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData, error);
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => {
            return {...prevState, [name]: file};
        });
    };

    const getFieldError = fieldName => {
        try {
            return error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    // const handleChange = event => {
    //     setCheckbox(event.target.checked);
    // };

    return (
        <Grid
            container
            direction="column"
            spacing={2}
            component="form"
            className={classes.root}
            autoComplete="off"
            onSubmit={submitFormHandler}
            noValidate
        >
            <FormElement
                label="Title"
                name="title"
                value={state.title}
                onChange={inputChangeHandler}
                error={getFieldError('title')}
                required
            />

            <FormElement
                label="Description"
                name="description"
                value={state.description}
                onChange={inputChangeHandler}
                error={getFieldError('description')}
                required
            />

            <Grid item xs>
                <TextField
                    type="file"
                    name="image"
                    onChange={fileChangeHandler}
                    error={Boolean(getFieldError('image'))}
                    helperText={getFieldError('image')}
                />
            </Grid>

            {/*<>*/}
            {/*    <Grid container justifyContent="center" alignItems="center" style={{textAlign: 'center'}}>*/}
            {/*        <Checkbox*/}
            {/*            sx={{my: '1.5em'}}*/}
            {/*            checked={checkbox}*/}
            {/*            onChange={handleChange}*/}
            {/*            inputProps={{'aria-label': 'controlled'}}*/}
            {/*        />*/}
            {/*        <Typography>*/}
            {/*            By submitting this form, you agree that the following information will be submitted to the public domain*/}
            {/*        </Typography>*/}
            {/*    </Grid>*/}
            {/*</>*/}

            <Grid item xs={12}>
                <ButtonWithProgress
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    loading={loading}
                    disabled={loading}
                >
                    Create
                </ButtonWithProgress>
            </Grid>
        </Grid>
    );
};

export default PlaceForm;