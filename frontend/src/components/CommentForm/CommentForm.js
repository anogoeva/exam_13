import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import {useDispatch, useSelector} from "react-redux";
import {addReviewRequest} from "../../store/actions/reviewsActions";
import {useNavigate, useParams} from "react-router-dom";
import {Rating} from "@mui/material";
import Typography from "@mui/material/Typography";

const useStyles = makeStyles(theme => ({
    root: {
        marginTop: theme.spacing(2)
    },
}));

const CommentForm = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const classes = useStyles();
    const user = useSelector(state => state.users.user);

    const [state, setState] = useState({
        comment: "",
        foodQuality: "",
        serviceQuality: "",
        interiorQuality: "",
    });

    const {id} = useParams();

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = {};
        Object.keys(state).forEach(key => {
            formData[key] = state[key];
        });
        formData.establishment = id;
        formData.comment = state.comment;
        dispatch(addReviewRequest({formData, navigate}));
        setState({comment: ""});
    };

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    return (
        <>
            {user ? (
                <Grid
                    container
                    direction="column"
                    spacing={2}
                    component="form"
                    className={classes.root}
                    autoComplete="off"
                    onSubmit={submitFormHandler}
                >
                    <Typography textAlign="left" component="legend">Food quality: </Typography>
                    <Rating name="foodQuality"
                            value={state.foodQuality || null}
                            onChange={(event, newValue) => {
                                setState(prevState => {
                                    return {...prevState, foodQuality: newValue};
                                });
                            }}/>

                    <Typography textAlign="left" component="legend">Service quality: </Typography>
                    <Rating name="serviceQuality"
                            value={state.serviceQuality || null}
                            onChange={(event, newValue) => {
                                setState(prevState => {
                                    return {...prevState, serviceQuality: newValue};
                                });
                            }}/>

                    <Typography textAlign="left" component="legend">Interior quality: </Typography>
                    <Rating name="interiorQuality"
                            value={state.interiorQuality || null}
                            onChange={(event, newValue) => {
                                setState(prevState => {
                                    return {...prevState, interiorQuality: newValue};
                                });
                            }}/>

                    <Grid item xs>
                        <TextField
                            fullWidth
                            multiline
                            rows={3}
                            variant="outlined"
                            label="Comment"
                            name="comment"
                            onChange={inputChangeHandler}
                            value={state.comment}
                        />
                    </Grid>

                    <Grid item xs>
                        <Button type="submit" color="primary" variant="contained">Add comment</Button>
                    </Grid>
                </Grid>
            ) : ""}
        </>
    );
};

export default CommentForm;