import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Container, Grid} from "@mui/material";
import {makeStyles} from "@mui/styles";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import {useNavigate, useParams} from "react-router-dom";
import theme from "../../theme";
import FileInput from "../UI/FileInput/FileInput";
import {addImageCafeRequest} from "../../store/actions/imagesCafeActions";

const useStyles = makeStyles(theme => ({
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    container: {
        width: "90%",
        margin: "0 auto",
        marginTop: theme.spacing(2),
        [theme.breakpoints.up('sm')]: {
            width: '60%',
        },
        [theme.breakpoints.up('md')]: {
            width: '50%',
        },
    },
}));

const UploadPhoto = () => {
    const navigate = useNavigate();
    const classes = useStyles();
    const dispatch = useDispatch();
    const loading = useSelector(state => state.imageCafes.addLoading);
    const [imageCafe, setImageCafe] = useState({
        image: null,
    });
    const { id } = useParams();
    const submitFormHandler = e => {
        e.preventDefault();

        const formData = new FormData();
        Object.keys(imageCafe).forEach(key => {
            formData.append(key, imageCafe[key]);
        });
        formData.append('establishment', id);
        dispatch(addImageCafeRequest({formData, navigate}));
        // console.log(formData.get('image'));
        setImageCafe({
            image: null,
        })
    };


    const fileChangeHandler = async (e) => {
        const name = e.target.name;
        const file = e.target.files[0]
        setImageCafe(prevState => {
            return {...prevState, [name]: file};
        });
    };

    return (
        <Container
            component="section"
            maxWidth="md"
            className={classes.container}
            style={{paddingTop: '150px'}}
        >
            <Grid
                container
                direction="column"
                spacing={2}
                component="form"
                autoComplete="off"
                onSubmit={submitFormHandler}
                noValidate
            >
                <h3 style={theme.title}>Upload new photo</h3>

                <Grid item xs>
                    <FileInput
                        required
                        label="Choose file"
                        type
                        name="image"
                        onChange={fileChangeHandler}
                    />
                </Grid>

                <Grid item xs={12}>
                    <ButtonWithProgress
                        type="submit"
                        variant="contained"
                        color="default"
                        className={classes.submit}
                        loading={loading}
                        disabled={loading}
                    >
                        Save
                    </ButtonWithProgress>
                </Grid>
            </Grid>
        </Container>
    );
};

export default UploadPhoto;