import {Card, CardHeader, CardMedia, Grid, makeStyles, TextField,} from "@material-ui/core";
import PropTypes from 'prop-types';
import imageNotAvailable from '../../assets/images/not_available.png';
import {apiURL} from "../../config";
import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import ButtonWithProgress from "../UI/ButtonWithProgress/ButtonWithProgress";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import AppWindow from "../UI/AppWindow/AppWindow";
import {deleteEstablishmentRequest} from "../../store/actions/establishmentsActions";

const useStyles = makeStyles(theme => ({
    card: {
        height: '100%'
    },
    media: {
        height: 0,
        paddingTop: '56.25%'
    },
    style: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 800,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    },
    button: {
        color: 'white',
        border: '2px solid #fff'
    },
    mediaQueriesDeleteBtn: {
        [theme.breakpoints.down('lg')]: {
            fontSize: '12px',
            padding: '8px 16px',
            textAlign: 'center',
        },
        [theme.breakpoints.down('md')]: {
            padding: '8px 37px',
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: '10px',
        },
    },
}));

const EstablishmentItem = ({title, image, overallReviews, id}) => {

    const dispatch = useDispatch();

    // const messagesEndRef = useRef(null);

    const classes = useStyles();

    let cardImage = imageNotAvailable;

    const [deleteElement, setDeleteElement] = useState('');

    // const imagesCafe = useSelector(state => state.imageCafes.imageCafe);

    if (image) {
        cardImage = apiURL + '/' + image;
    }

    const [open, setOpen] = React.useState(false);
    // const handleOpen = () => setOpen(true);
    // const handleClose = () => setOpen(false);

    // useEffect(() => {
    //     window.scrollTo(0, 0);
    //     dispatch(fetchImageCafeRequest('?establishment=' + id));
    // }, [dispatch, messagesEndRef, id]);

    const user = useSelector(state => state.users.user);

    const fetchLoading = useSelector(state => state.establishments.fetchLoading);

    const deleteEstablishment = id => {
        dispatch(deleteEstablishmentRequest(id));
        setOpen(false);
        setDeleteElement(prevState => {
            prevState = '';
            return prevState
        });
    };

    return (
        <Grid item xs={12} sm={12} md={6} lg={4}>
            <Card className={classes.card}>
                <CardHeader component={Link} to={'/establishments/' + id} title={title}/>
                <CardMedia
                    image={cardImage}
                    // onClick={handleOpen}
                    title={title}
                    className={classes.media}
                />

                <TextField
                    value={'Rating: ' + overallReviews}
                />

                {/*{*/}
                {/*    imagesCafe.map(image => (*/}
                {/*        <TextField*/}
                {/*            key={image._id}*/}
                {/*            value={imagesCafe ? 'Photos: ' + imagesCafe.length : 'Photos: ' + 0}*/}
                {/*        />*/}
                {/*    ))*/}
                {/*}*/}
                {user && user.role === 'admin' ?
                    <Grid container>
                        <Grid item xs={12} sm={4} md={4} lg={3} className={classes.gridCenter}>
                            <ButtonWithProgress
                                type="submit"
                                fullWidth
                                variant="outlined"
                                startIcon={<DeleteOutlinedIcon/>}
                                color="default"
                                className={classes.mediaQueriesDeleteBtn}
                                loading={fetchLoading}
                                disabled={fetchLoading}
                                onClick={() => {
                                    setOpen(true);
                                    setDeleteElement(prevState => {
                                        prevState = id;
                                        return prevState
                                    });
                                }}
                            >
                                Удалить
                            </ButtonWithProgress>
                        </Grid>
                    </Grid> : ''}

            </Card>

            <AppWindow open={open} onClose={() => {
                setOpen(false);
                setDeleteElement(prevState => {
                    prevState = '';
                    return prevState
                });
            }} confirm={() => deleteEstablishment(deleteElement)}/>
        </Grid>
    );
};

EstablishmentItem.propTypes = {
    id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string.isRequired,

};

export default EstablishmentItem;