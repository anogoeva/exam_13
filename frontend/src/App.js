import Layout from "./components/UI/Layout/Layout";
import {Route, Routes} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import {addEstablishment, login, oneEstablishments, register, root} from "./paths";
import CafeCritics from "./containers/CafeCritics/CafeCritics";
import OneEstablishment from "./containers/OneEstablishment/OneEstablishment";
import NewPlace from "./containers/NewPlace/NewPlace";
import ProtectedRoutesForAdmin from "./components/ProtectedRoutes/ProtectedRoutesForAdmin/ProtectedRoutesForAdmin";


const App = () => (
    <Layout>
        <Routes>
            <Route path={root} element={<CafeCritics/>}/>
            <Route path={register} element={<Register/>}/>
            <Route path={login} element={<Login/>}/>
            <Route path={oneEstablishments} element={<OneEstablishment/>}/>
            <Route element={<ProtectedRoutesForAdmin/>}>
                <Route path={addEstablishment} element={<NewPlace/>}/>
            </Route>
        </Routes>
    </Layout>
);

export default App;
