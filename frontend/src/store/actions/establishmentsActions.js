import establishmentsSlice from "../slices/establishmentsSlice";

export const {
    fetchEstablishmentRequest,
    fetchEstablishmentSuccess,
    fetchEstablishmentFailure,
    addEstablishmentRequest,
    addEstablishmentSuccess,
    addEstablishmentFailure,
    fetchOneEstablishmentRequest,
    fetchOneEstablishmentSuccess,
    fetchOneEstablishmentFailure,
    deleteEstablishmentRequest,
    deleteEstablishmentSuccess,
    deleteEstablishmentFailure,
} = establishmentsSlice.actions;