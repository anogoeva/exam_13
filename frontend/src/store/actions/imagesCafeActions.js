import imageCafesSlice from "../slices/imageCafesSlice";

export const {
    fetchImageCafeRequest,
    fetchImageCafeSuccess,
    fetchImageCafeFailure,
    addImageCafeRequest,
    addImageCafeSuccess,
    addImageCafeFailure,
    fetchOneImageCafeRequest,
    fetchOneImageCafeSuccess,
    fetchOneImageCafeFailure,
    deleteImageCafeRequest,
    deleteImageCafeSuccess,
    deleteImageCafeFailure,
} = imageCafesSlice.actions;