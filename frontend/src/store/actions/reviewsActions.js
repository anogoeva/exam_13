import reviewsSlice from "../slices/reviewsSlice";

export const {
    fetchReviewRequest,
    fetchReviewSuccess,
    fetchReviewFailure,
    addReviewRequest,
    addReviewSuccess,
    addReviewFailure,
    fetchOneReviewRequest,
    fetchOneReviewSuccess,
    fetchOneReviewFailure,
    deleteReviewRequest,
    deleteReviewSuccess,
    deleteReviewFailure,
} = reviewsSlice.actions;