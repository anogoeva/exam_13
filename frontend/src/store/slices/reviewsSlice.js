import {createSlice} from "@reduxjs/toolkit";

const name = 'review';

const initialState = {
    review: [],
    oneReview: {},
    singleLoading: false,
    fetchLoading: false,
    addLoading: false,
    addError: null,
}

const reviewSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchReviewRequest(state) {
            state.fetchLoading = true;
        },
        fetchReviewSuccess(state, action) {
            state.review = action.payload;
            state.fetchLoading = false;
        },
        fetchReviewFailure(state) {
            state.fetchLoading = false;
        },
        addReviewRequest(state) {
            state.addLoading = true;
        },
        addReviewSuccess(state) {
            state.addLoading = false;
            state.addError = null;
        },
        addReviewFailure(state, action) {
            state.addLoading = false;
            state.addError = action.payload;
        },
        fetchOneReviewRequest(state) {
            state.singleLoading = true;
        },
        fetchOneReviewSuccess(state, action) {
            state.oneReview = action.payload;
            state.singleLoading = false;
        },
        fetchOneReviewFailure(state) {
            state.singleLoading = false;
        },
        clearReviewErrors(state) {
            state.addError = null;
        },
        deleteReviewRequest(state) {
            state.deleteLoading = true;
        },
        deleteReviewSuccess(state, {payload: reviewId}) {
            state.deleteLoading = false;
            state.deleteError = null;
            state.review = state.review.filter(review => review._id !== reviewId);
        },
        deleteReviewFailure(state, action) {
            state.deleteLoading = false;
            state.deleteError = action.payload;
        },
    }
});

export default reviewSlice;