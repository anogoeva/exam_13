import {createSlice} from "@reduxjs/toolkit";

const name = 'establishment';

const initialState = {
    establishment: [],
    oneEstablishment: {},
    singleLoading: false,
    fetchLoading: false,
    addLoading: false,
    addError: null,
}

const establishmentSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchEstablishmentRequest(state) {
            state.fetchLoading = true;
        },
        fetchEstablishmentSuccess(state, action) {
            state.establishment = action.payload;
            state.fetchLoading = false;
        },
        fetchEstablishmentFailure(state) {
            state.fetchLoading = false;
        },
        addEstablishmentRequest(state) {
            state.addLoading = true;
        },
        addEstablishmentSuccess(state) {
            state.addLoading = false;
            state.addError = null;
        },
        addEstablishmentFailure(state, action) {
            state.addLoading = false;
            state.addError = action.payload;
        },
        fetchOneEstablishmentRequest(state) {
            state.singleLoading = true;
        },
        fetchOneEstablishmentSuccess(state, action) {
            state.oneEstablishment = action.payload;
            state.singleLoading = false;
        },
        fetchOneEstablishmentFailure(state) {
            state.singleLoading = false;
        },
        clearEstablishmentErrors(state) {
            state.addError = null;
        },
        deleteEstablishmentRequest(state) {
            state.deleteLoading = true;
        },
        deleteEstablishmentSuccess(state, {payload: establishmentId}) {
            state.deleteLoading = false;
            state.deleteError = null;
            state.establishment = state.establishment.filter(establishment => establishment._id !== establishmentId);
        },
        deleteEstablishmentFailure(state, action) {
            state.deleteLoading = false;
            state.deleteError = action.payload;
        },
    }
});

export default establishmentSlice;