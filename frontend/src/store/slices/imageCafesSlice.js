import {createSlice} from "@reduxjs/toolkit";

const name = 'imageCafe';

const initialState = {
    imageCafe: [],
    oneImageCafe: {},
    singleLoading: false,
    fetchLoading: false,
    addLoading: false,
    addError: null,
}

const imageCafeSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchImageCafeRequest(state) {
            state.fetchLoading = true;
        },
        fetchImageCafeSuccess(state, action) {
            state.imageCafe = action.payload;
            state.fetchLoading = false;
        },
        fetchImageCafeFailure(state) {
            state.fetchLoading = false;
        },
        addImageCafeRequest(state) {
            state.addLoading = true;
        },
        addImageCafeSuccess(state) {
            state.addLoading = false;
            state.addError = null;
        },
        addImageCafeFailure(state, action) {
            state.addLoading = false;
            state.addError = action.payload;
        },
        fetchOneImageCafeRequest(state) {
            state.singleLoading = true;
        },
        fetchOneImageCafeSuccess(state, action) {
            state.oneImageCafe = action.payload;
            state.singleLoading = false;
        },
        fetchOneImageCafeFailure(state) {
            state.singleLoading = false;
        },
        clearImageCafeErrors(state) {
            state.addError = null;
        },
        deleteImageCafeRequest(state) {
            state.deleteLoading = true;
        },
        deleteImageCafeSuccess(state, {payload: imageCafeId}) {
            state.deleteLoading = false;
            state.deleteError = null;
            state.imageCafe = state.imageCafe.filter(imageCafe => imageCafe._id !== imageCafeId);
        },
        deleteImageCafeFailure(state, action) {
            state.deleteLoading = false;
            state.deleteError = action.payload;
        },
    }
});

export default imageCafeSlice;