import {
    changePasswordFailure,
    changePasswordRequest,
    changePasswordSuccess,
    fetchUsersFailure,
    fetchUsersRequest,
    fetchUsersSuccess,
    forgotPasswordFailure,
    forgotPasswordRequest,
    forgotPasswordSuccess,
    loginUser,
    loginUserFailure,
    loginUserSuccess,
    logout,
    registerUser,
    registerUserFailure,
    registerUserSuccess,
    resetPasswordFailure,
    resetPasswordRequest,
    resetPasswordSuccess,
} from "../actions/usersActions";
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {root, userLogin} from "../../paths";
import {put, takeEvery} from "redux-saga/effects";
import History from '../../History';


export function* registerUserSaga({payload}) {
    try {
        const userData = {
            email: payload.email,
            username: payload.username,
            password: payload.password,
            role: payload.role || 'user',
        };

        const response = yield axiosApi.post('/users', userData);
        if (response.data) {
            yield put(registerUserSuccess(response.data));
            toast.success('Вы зарегистрированы');
            History.push(root);
        }
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(registerUserFailure(e.response.data));
    }
}


export function* loginUserSaga({payload: user}) {
    try {
        const response = yield axiosApi.post('/users/sessions', user);
        user.navigate('/', true);
        yield put(loginUserSuccess(response.data));
        toast.success('Вы авторизированы!');
        History.push(root);
    } catch (e) {
        toast.error(e.response.data.global);
        yield put(loginUserFailure(e.response.data));
    }
}


export function* fetchUserSaga() {
    try {
        const response = yield axiosApi.get(`/users`);
        yield put(fetchUsersSuccess(response.data));
    } catch (e) {
        toast.error(e.response.data.error);
        yield put(fetchUsersFailure(e.response.data.error));
    }
}


export function* resetPasswordSaga({payload: user}) {
    try {
        const response = yield axiosApi.post('/users/reset', user);
        user.navigate(userLogin, true);
        yield put(resetPasswordSuccess());
        toast.success(response.data?.message);
    } catch (e) {
        yield put(resetPasswordFailure(e.response.data));
    }
}


export function* changePasswordSaga({payload: user}) {
    try {
        const response = yield axiosApi.post('/users/change', user);
        user.navigate('/', true);
        yield put(changePasswordSuccess());
        toast.success(response.data?.message);
    } catch (e) {
        yield put(changePasswordFailure(e.response.data));
    }
}


export function* forgotPasswordSaga({payload: user}) {
    try {
        const response = yield axiosApi.post('/users/forgot', user);
        user.navigate('/', true);
        yield put(forgotPasswordSuccess());
        toast.success(response.data?.message);
    } catch (e) {
        yield put(forgotPasswordFailure(e.response.data));
    }
}

export function* logoutUserSaga() {
    try {
        yield axiosApi.delete('/users/sessions');
    } catch (e) {
        if (e.response && e.response.data) {
            yield  put(loginUserFailure(e.response.data));
        } else {
            yield put(loginUserFailure({message: "No internet connexion"}));
        }
    }
}

const usersSaga = [
    takeEvery(registerUser, registerUserSaga),
    takeEvery(loginUser, loginUserSaga),
    takeEvery(logout, logoutUserSaga),
    takeEvery(fetchUsersRequest, fetchUserSaga),
    takeEvery(resetPasswordRequest, resetPasswordSaga),
    takeEvery(changePasswordRequest, changePasswordSaga),
    takeEvery(forgotPasswordRequest, forgotPasswordSaga),
];

export default usersSaga;