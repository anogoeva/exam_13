import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {
    addImageCafeFailure,
    addImageCafeRequest,
    addImageCafeSuccess,
    deleteImageCafeFailure,
    deleteImageCafeRequest,
    deleteImageCafeSuccess,
    fetchImageCafeFailure,
    fetchImageCafeRequest,
    fetchImageCafeSuccess,
    fetchOneImageCafeFailure,
    fetchOneImageCafeRequest,
    fetchOneImageCafeSuccess
} from "../actions/imagesCafeActions";

export function* establishmentSagas(query) {
    try {
        const response = yield axiosApi.get('/images/' + query.payload);
        yield put(fetchImageCafeSuccess(response.data));
    } catch (e) {
        toast.error('Can not load images');
        yield put(fetchImageCafeFailure());
    }
}

export function* oneImageCafeSagas({payload: id}) {
    try {
        const response = yield axiosApi.get('/images/' + id);
        yield put(fetchOneImageCafeSuccess(response.data));
    } catch (e) {
        toast.error('Can not load image');
        yield put(fetchOneImageCafeFailure());
    }
}

export function* addImageCafeSaga({payload}) {
    try {
        yield axiosApi.post('/images', payload.formData);
        yield put(addImageCafeSuccess());
        yield put(fetchImageCafeRequest());
        toast.success('Image added');
        payload.navigate('/');
    } catch (error) {
        yield put(addImageCafeFailure(error.response.data));
    }
}

function* deleteImageCafeSaga({payload: id}) {
    try {
        yield axiosApi.delete(`/images/${id}`);
        yield put(deleteImageCafeSuccess(id));
        toast.success('Image removed');
    } catch (e) {
        yield put(deleteImageCafeFailure(e.response.data));
    }
}

const establishmentSaga = [
    takeEvery(fetchImageCafeRequest, establishmentSagas),
    takeEvery(addImageCafeRequest, addImageCafeSaga),
    takeEvery(fetchOneImageCafeRequest, oneImageCafeSagas),
    takeEvery(deleteImageCafeRequest, deleteImageCafeSaga),
];

export default establishmentSaga;