import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {
    addReviewFailure,
    addReviewRequest,
    addReviewSuccess,
    deleteReviewFailure,
    deleteReviewRequest,
    deleteReviewSuccess,
    fetchReviewFailure,
    fetchReviewRequest,
    fetchReviewSuccess,
    fetchOneReviewFailure,
    fetchOneReviewRequest,
    fetchOneReviewSuccess
} from "../actions/reviewsActions";

export function* reviewSagas(query) {
    try {
        const response = yield axiosApi.get('/reviews/' + query.payload);
        yield put(fetchReviewSuccess(response.data));
    } catch (e) {
        toast.error('Can not load reviews');
        yield put(fetchReviewFailure());
    }
}

export function* oneReviewSagas({payload: id}) {
    try {
        const response = yield axiosApi.get('/reviews/' + id);
        yield put(fetchOneReviewSuccess(response.data));
    } catch (e) {
        toast.error('Can not load review');
        yield put(fetchOneReviewFailure());
    }
}

export function* addReviewSaga({payload}) {
    try {
        yield axiosApi.post('/reviews', payload.formData);
        yield put(addReviewSuccess());
        yield put(fetchReviewRequest());
        toast.success('Review added');
        payload.navigate('/');
    } catch (error) {
        yield put(addReviewFailure(error.response.data));
    }
}

function* deleteReviewSaga({payload: id}) {
    try {
        yield axiosApi.delete(`/reviews/${id}`);
        yield put(deleteReviewSuccess(id));
        toast.success('Review removed');
    } catch (e) {
        yield put(deleteReviewFailure(e.response.data));
    }
}

const reviewSaga = [
    takeEvery(fetchReviewRequest, reviewSagas),
    takeEvery(addReviewRequest, addReviewSaga),
    takeEvery(fetchOneReviewRequest, oneReviewSagas),
    takeEvery(deleteReviewRequest, deleteReviewSaga),
];

export default reviewSaga;