import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";
import {toast} from "react-toastify";
import {
    addEstablishmentFailure,
    addEstablishmentRequest,
    addEstablishmentSuccess,
    deleteEstablishmentFailure,
    deleteEstablishmentRequest,
    deleteEstablishmentSuccess,
    fetchEstablishmentFailure,
    fetchEstablishmentRequest,
    fetchEstablishmentSuccess,
    fetchOneEstablishmentFailure,
    fetchOneEstablishmentRequest,
    fetchOneEstablishmentSuccess
} from "../actions/establishmentsActions";
import History from "../../History";
import {root} from "../../paths";

export function* establishmentSagas() {
    try {
        const response = yield axiosApi.get('/establishments');
        yield put(fetchEstablishmentSuccess(response.data));
    } catch (e) {
        toast.error('Can not load establishments');
        yield put(fetchEstablishmentFailure());
    }
}

export function* oneEstablishmentSagas({payload: id}) {
    try {
        const response = yield axiosApi.get('/establishments/' + id);
        yield put(fetchOneEstablishmentSuccess(response.data));
    } catch (e) {
        toast.error('Can not load establishment');
        yield put(fetchOneEstablishmentFailure());
    }
}

export function* addEstablishmentSaga({payload}) {
    try {
        yield axiosApi.post('/establishments', payload);
        yield put(addEstablishmentSuccess());
        yield put(fetchEstablishmentRequest());
        toast.success('Establishment added');
        History.push(root);
    } catch (error) {
        yield put(addEstablishmentFailure(error.response.data));
    }
}

function* deleteEstablishmentSaga({payload: id}) {
    try {
        yield axiosApi.delete(`/establishments/${id}`);
        yield put(deleteEstablishmentSuccess(id));
        toast.success('Establishments removed');
    } catch (e) {
        yield put(deleteEstablishmentFailure(e.response.data));
    }
}

const establishmentSaga = [
    takeEvery(fetchEstablishmentRequest, establishmentSagas),
    takeEvery(addEstablishmentRequest, addEstablishmentSaga),
    takeEvery(fetchOneEstablishmentRequest, oneEstablishmentSagas),
    takeEvery(deleteEstablishmentRequest, deleteEstablishmentSaga),
];

export default establishmentSaga;