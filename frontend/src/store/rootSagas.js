import {all} from 'redux-saga/effects';
import usersSagas from "./sagas/usersSagas";
import establishmentsSagas from "./sagas/establishmentsSagas";
import imageCafesSagas from "./sagas/imageCafesSagas";
import reviewSagas from "./sagas/reviewsSagas";

export function* rootSagas() {
    yield all([
        ...usersSagas,
        ...establishmentsSagas,
        ...imageCafesSagas,
        ...reviewSagas,
    ]);
}