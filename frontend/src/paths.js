export const root = '/';
export const register = '/register';
export const login = '/login';
export const userLogin = "/user/login";
export const addEstablishment = "/addEstablishment";
export const oneEstablishments = "/establishments/:id";