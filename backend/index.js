const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const config = require('./config');
const users = require('./app/users');
const establishments = require('./app/establishments');
const images = require('./app/images');
const reviews = require('./app/reviews');


const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.use('/users', users);
app.use('/establishments', establishments);
app.use('/images', images);
app.use('/reviews', reviews);

const run = async () => {
  await mongoose.connect(config.db.url);

  app.listen(config.port, () => {
    console.log(`Server started on ${config.port} port!`);
  });

  exitHook(() => {
    console.log('exiting');
    mongoose.disconnect();
  });
};

run().catch(e => console.error(e));

