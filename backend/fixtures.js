const mongoose = require("mongoose");
const config = require("./config");
const User = require("./models/User");
const {nanoid} = require("nanoid");
const Establishment = require("./models/Establishment");
const ImageCafe = require("./models/ImageCafe");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create(
    {
      email: "user@gmail.com",
      password: "12345678",
      role: "user",
      username: "user",
      token: nanoid(),
    },
    {
      email: "admin@gmail.com",
      password: "12345678",
      role: "admin",
      username: "admin",
      token: nanoid(),
    },
  );

  const [establishment01, establishment02, establishment03, establishment04] = await Establishment.create(
    {
      title: "Первое заведение",
      description: "Национальная кухня прекрасна",
      user: user,
      overallReviews: 5,
      foodReviews: 5,
      serviceReviews: 5,
      interiorReviews: 5,
      image: 'fixtures/01.jpeg'
    },
    {
      title: "Второе заведение",
      description: "Китайская кухня ужасна",
      user: user,
      overallReviews: 4,
      foodReviews: 4,
      serviceReviews: 4,
      interiorReviews: 4,
      image: 'fixtures/02.jpeg'
    },
    {
      title: "Третье заведение",
      description: "Европейская кухня ужасна",
      user: admin,
      overallReviews: 3,
      foodReviews: 3,
      serviceReviews: 3,
      interiorReviews: 3,
      image: 'fixtures/03.jpeg'
    },
    {
      title: "Четвертое заведение",
      description: "Индийская кухня ужасна",
      user: admin,
      overallReviews: 4,
      foodReviews: 4,
      serviceReviews: 4,
      interiorReviews: 4,
      image: 'fixtures/04.jpeg'
    },
  );

  await ImageCafe.create(

    {
      image: "fixtures/food-01.jpg",
      establishment: establishment01,
      user: user,
    },
    {
      image: "fixtures/food-02.jpg",
      establishment: establishment01,
      user: user,
    },
    {
      image: "fixtures/food-03.jpg",
      establishment: establishment01,
      user: admin,
    },

    {
      image: "fixtures/food-04.jpeg",
      establishment: establishment02,
      user: user,
    },
    {
      image: "fixtures/food-05.jpg",
      establishment: establishment02,
      user: user,
    },
    {
      image: "fixtures/food-06.jpeg",
      establishment: establishment02,
      user: admin,
    },

    {
      image: "fixtures/food-07.jpg",
      establishment: establishment03,
      user: user,
    },
    {
      image: "fixtures/food-08.jpeg",
      establishment: establishment03,
      user: user,
    },
    {
      image: "fixtures/food-09.jpg",
      establishment: establishment03,
      user: admin,
    },

    {
      image: "fixtures/food-10.jpeg",
      establishment: establishment04,
      user: user,
    },
    {
      image: "fixtures/food-11.jpeg",
      establishment: establishment04,
      user: user,
    },
    {
      image: "fixtures/food-12.jpeg",
      establishment: establishment04,
      user: admin,
    },
  );

  await mongoose.connection.close();
};

run().catch(console.error);

