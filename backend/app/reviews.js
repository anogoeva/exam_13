const express = require('express');
const auth = require("../middleware/auth");
const Establishment = require("../models/Establishment");
const permit = require("../middleware/permit");
const User = require("../models/User");
const Review = require("../models/Review");
const ImageCafe = require("../models/ImageCafe");

const router = express.Router();

router.get('/', async (req, res) => {
  try {
    const query = {};
    if (req.query.establishment) {
      query.establishment = req.query.establishment;
    }
    const reviews = await Review.find((query)).populate('user', 'username').sort({dateTime: -1});
    res.send(reviews);
  } catch (e) {
    res.status(500).send(e);
  }
});


router.get('/:id', async (req, res) => {
  try {
    const review = await Review.findById(req.params.id);

    if (review) {
      res.send(review);
    } else {
      res.status(404).send({error: 'Review not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});


router.post('/', auth, permit('admin', 'user'), async (req, res) => {
  try {

    const token = req.get('Authorization');
    const user = await User.findOne({token});

    const reviewData = {
      comment: req.body.comment,
      establishment: req.body.establishment,
      user: user._id,
      foodQuality: req.body.foodQuality,
      serviceQuality: req.body.serviceQuality,
      interiorQuality: req.body.interiorQuality,
    };

    const review = new Review(reviewData);
    await review.save();
    res.send(review);
  } catch (error) {
    console.log(error)
    res.status(400).send(error);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const review = await Review.findByIdAndRemove(req.params.id);

    if (review) {
      res.send(`Review was removed'`);
    } else {
      res.status(404).send({error: 'Review not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;