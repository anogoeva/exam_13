const express = require('express');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");
const User = require("../models/User");
const multer = require("multer");
const path = require("path");
const config = require('../config');
const {nanoid} = require("nanoid");
const ImageCafe = require("../models/ImageCafe");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
  try {
    const query = {};
    if (req.query.establishment) {
      query.establishment = req.query.establishment;
    }
    const images = await ImageCafe.find((query));
    res.send(images);
  } catch (e) {
    res.status(500).send(e);
  }
});


router.get('/:id', async (req, res) => {
  try {
    const image = await ImageCafe.findById(req.params.id);

    if (image) {
      res.send(image);
    } else {
      res.status(404).send({error: 'Establishment not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});


router.post('/', auth, permit('admin', 'user'), upload.single('image'), async (req, res) => {
  try {

    const token = req.get('Authorization');
    const user = await User.findOne({token});

    const imageData = {
      establishment: req.body.establishment,
      user: user._id,
    };

    if (req.file) {
      imageData.image = 'uploads/' + req.file.filename;
    }

    const image = new ImageCafe(imageData);
    await image.save();
    res.send(image);
  } catch (error) {
    console.log(error)
    res.status(400).send(error);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const image = await ImageCafe.findByIdAndRemove(req.params.id);

    if (image) {
      res.send(`Image removed'`);
    } else {
      res.status(404).send({error: 'Image not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;