const express = require('express');
const User = require('../models/User');
const permit = require("../middleware/permit");
const auth = require("../middleware/auth");
const {nanoid} = require("nanoid");

const router = express.Router();

router.get('/', auth, permit('admin'), async (req, res) => {
  try {
    const users = await User.find({role: 'user'})
      .select('username email group');
    res.send(users);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.get('/notification', auth, permit('admin'), async (req, res) => {
  try {
    const notification = await User.findById(req.user._id)
      .select('notification');
    res.send(notification);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.post('/', async (req, res) => {
  try {

    const user = new User({
      email: req.body.email,
      password: req.body.password,
      username: req.body.username,
      role: req.body.role,
    });

    user.generateToken();
    await user.save();

    res.send(user);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.put('/notification', auth, permit('admin'), async (req, res) => {
  try {
    const notification = await User.findById(req.user._id);
    notification.notification = req.body.payload;

    notification.save({validateBeforeSave: false});
    res.send(notification.notification);
  } catch (e) {
    res.status(500).send(e);
  }
});

router.post('/sessions', async (req, res) => {
  let user = await User.findOne({username: req.body.username});

  if (!user) {
    return res.status(401).send({message: 'Пожалуйста, введите корректные данные'});
  }

  const isMatch = await user.checkPassword(req.body.password);

  if (!isMatch) {
    return res.status(401).send({message: 'Пожалуйста, введите корректные данные'});
  }

  user.generateToken();
  await user.save({validateBeforeSave: false});

  user = await User.findOne({username: req.body.username})
    .select('token role username');

  res.send(user);
});

router.post('/forgot', async (req, res) => {
  try {
    const user = await User.findOne({email: req.body.email});

    if (!user) return res.status(404).send({message: 'Такая почта не найдена'});

    const resetCode = nanoid(8);
    await User.findOneAndUpdate({email: user.email}, {resetCode});
    await sendMail({email: user.email}, 'Сброс пароля', null, emailDistribution.passwordReset(resetCode, user.name))
    res.send({message: "Код сброса пароля отправлен на почту!"})

    const userToBeUpdated = await User.findOne({resetCode: resetCode});

    const deleteResetCode = async ()=>{
      if(userToBeUpdated){
        await User.findByIdAndUpdate(userToBeUpdated._id,{resetCode: ''});
      }
    }

    setTimeout(deleteResetCode,300000);

  } catch (e) {
    res.status(500).send(e);
  }
});


router.post('/reset', async (req, res) => {
  try {
    let user = await User.findOne({resetCode: req.body.secretCode});
    if (!user) {
      return res.status(404).send({message: 'Неправильный код'})
    }
    user.password = req.body.password

    user.$ignore('email');
    await user.save()

    res.send({message: " Пароль успешно изменен"});
  } catch (e) {
    console.log(e.message)
    res.status(500).send(e);
  }
})

router.post('/change', auth, async (req, res) => {
  try {
    let user = await User.findById(req.user._id);
    if (!user) {
      return res.status(401).send({message: 'Доступ запрещен'})
    }
    user.password = req.body.password
    user.$ignore('email');
    await user.save()

    res.send({message: " Пароль успешно изменен"});
  } catch (e) {
    res.status(500).send(e);
  }
})

router.delete('/sessions', async (req, res) => {
  const token = req.get('Authorization');
  const success = {message: 'Success'};

  if (!token) return res.send(success);

  const user = await User.findOne({token});

  if (!user) return res.send(success);

  user.generateToken();

  await user.save({validateBeforeSave: false});

  return res.send(success);
});

module.exports = router;