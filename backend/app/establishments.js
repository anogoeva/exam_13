const express = require('express');
const auth = require("../middleware/auth");
const Establishment = require("../models/Establishment");
const permit = require("../middleware/permit");
const User = require("../models/User");
const multer = require("multer");
const path = require("path");
const config = require('../config');
const {nanoid} = require("nanoid");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
  try {
    const establishments = await Establishment.find({});
    res.send(establishments);
  } catch (e) {
    res.status(500).send(e);
  }
});


router.get('/:id', async (req, res) => {
  try {
    const establishment = await Establishment.findById(req.params.id);

    if (establishment) {
      res.send(establishment);
    } else {
      res.status(404).send({error: 'Establishment not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});


router.post('/', auth, permit('admin', 'user'), upload.single('image'), async (req, res) => {
  try {

    const token = req.get('Authorization');
    const user = await User.findOne({token});

    const establishmentData = {
      title: req.body.title,
      description: req.body.description,
      user: user._id,
    };

    if (req.file) {
      establishmentData.image = 'uploads/' + req.file.filename;
    }

    const establishment = new Establishment(establishmentData);
    await establishment.save();
    res.send(establishment);
  } catch (error) {
    console.log(error)
    res.status(400).send(error);
  }
});

router.delete('/:id', auth, permit('admin'), async (req, res) => {
  try {
    const establishment = await Establishment.findByIdAndRemove(req.params.id);

    if (establishment) {
      res.send(`Establishment '${establishment.title} removed'`);
    } else {
      res.status(404).send({error: 'Establishment not found'});
    }
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;