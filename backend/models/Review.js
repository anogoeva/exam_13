const mongoose = require('mongoose');

const ReviewSchema = new mongoose.Schema({
    comment: {
      type: String,
      required: 'Comment is required',
    },
    establishment: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Establishment',
      required: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    foodQuality: {
      type: Number,
      required: true,
    },
    serviceQuality: {
      type: Number,
      required: true,
    },
    interiorQuality: {
      type: Number,
      required: true,
    },
    dateTime: {
      type: Date,
      default: Date.now(),
    }
  }
);

const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;

