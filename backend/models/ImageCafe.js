const mongoose = require('mongoose');

const ImageSchema = new mongoose.Schema({
    image: {
      type: String,
      required: true,
    },
    establishment: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Establishment',
      required: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
  }
);

const ImageCafe = mongoose.model('Image', ImageSchema);

module.exports = ImageCafe;

