const mongoose = require('mongoose');

const EstablishmentSchema = new mongoose.Schema({
    title: {
      type: String,
      required: true,
      unique: true,
    },
    description: {
      type: String,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    overallReviews: {
      type: 'Number',
      default: 0,
    },
    foodReviews: {
      type: 'Number',
      default: 0,
    },
    serviceReviews: {
      type: 'Number',
      default: 0,
    },
    interiorReviews: {
      type: 'Number',
      default: 0,
    },
    image: {
      type: String,
      required: false
    },
  }
);

const Establishment = mongoose.model('Establishment', EstablishmentSchema);

module.exports = Establishment;

