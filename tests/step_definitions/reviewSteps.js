Then('я зашёл на страницу заведения', () => {
    I.click(`//span[contains(text(),'Первое заведение')]`);
    I.wait(3);
});

Then('я ставлю рейтинг еды', () => {
    I.click(`//form/span[1]/label[5]/span[1]`);
    I.wait(2);
});

Then('я ставлю рейтинг сервиса', () => {
    I.click(`//form/span[2]/label[5]/span[1]`);
    I.wait(2);
});

Then('я ставлю рейтинг интерьера', () => {
    I.click(`//form/span[3]/label[5]/span[1]`);
    I.wait(2);
});

Then('я введу данные:', (table) => {
    table.rows.forEach(row => {
        const name = row.cells[0].value;
        const value = row.cells[1].value;

        I.fillField(name, value);
    });
    I.wait(4)
});

When('нажимаю на кнопку {string}', (button) => {
    I.click(button);
    I.wait(1);
});