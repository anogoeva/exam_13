const { I } = inject();

Given('я зашёл на страницу {string}', (page) => {
    I.amOnPage('/' + page);
});

Given('я ввожу данные:', (table) => {
    table.rows.forEach(row => {
        const name = row.cells[0].value;
        const value = row.cells[1].value;

        I.fillField(name, value);
    });
});

When('нажимаю на кнопку регистрации', () => {
    I.click(`//button/span[contains(text(),'Sign up')]`);
    I.wait(1);
});

When('нажимаю на кнопку логина', () => {
    I.click(`//button/span[contains(text(),'Log in')]`);
    I.wait(1);
});

Then('я вижу текст {string}', (text) => {
    I.see(text);
});